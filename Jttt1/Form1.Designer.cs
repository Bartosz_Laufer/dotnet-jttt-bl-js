﻿namespace Jttt1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonAdd = new System.Windows.Forms.Button();
            this.textBoxUrl = new System.Windows.Forms.TextBox();
            this.textBoxText = new System.Windows.Forms.TextBox();
            this.textBoxEmail = new System.Windows.Forms.TextBox();
            this.labelUrl = new System.Windows.Forms.Label();
            this.labelText = new System.Windows.Forms.Label();
            this.labelEmail = new System.Windows.Forms.Label();
            this.labelJttt = new System.Windows.Forms.Label();
            this.listTasks = new System.Windows.Forms.ListBox();
            this.buttonStart = new System.Windows.Forms.Button();
            this.buttonDeserialize = new System.Windows.Forms.Button();
            this.buttonSerialize = new System.Windows.Forms.Button();
            this.buttonClear = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonAdd
            // 
            this.buttonAdd.Location = new System.Drawing.Point(172, 305);
            this.buttonAdd.Name = "buttonAdd";
            this.buttonAdd.Size = new System.Drawing.Size(109, 51);
            this.buttonAdd.TabIndex = 0;
            this.buttonAdd.Text = "ADD";
            this.buttonAdd.UseVisualStyleBackColor = true;
            this.buttonAdd.Click += new System.EventHandler(this.ButtonAdd_Click);
            // 
            // textBoxUrl
            // 
            this.textBoxUrl.Location = new System.Drawing.Point(135, 77);
            this.textBoxUrl.Name = "textBoxUrl";
            this.textBoxUrl.Size = new System.Drawing.Size(202, 20);
            this.textBoxUrl.TabIndex = 1;
            // 
            // textBoxText
            // 
            this.textBoxText.Location = new System.Drawing.Point(135, 125);
            this.textBoxText.Name = "textBoxText";
            this.textBoxText.Size = new System.Drawing.Size(202, 20);
            this.textBoxText.TabIndex = 2;
            // 
            // textBoxEmail
            // 
            this.textBoxEmail.Location = new System.Drawing.Point(135, 177);
            this.textBoxEmail.Name = "textBoxEmail";
            this.textBoxEmail.Size = new System.Drawing.Size(202, 20);
            this.textBoxEmail.TabIndex = 3;
            // 
            // labelUrl
            // 
            this.labelUrl.AutoSize = true;
            this.labelUrl.Location = new System.Drawing.Point(60, 77);
            this.labelUrl.Name = "labelUrl";
            this.labelUrl.Size = new System.Drawing.Size(32, 13);
            this.labelUrl.TabIndex = 4;
            this.labelUrl.Text = "URL:";
            // 
            // labelText
            // 
            this.labelText.AutoSize = true;
            this.labelText.Location = new System.Drawing.Point(60, 131);
            this.labelText.Name = "labelText";
            this.labelText.Size = new System.Drawing.Size(33, 13);
            this.labelText.TabIndex = 5;
            this.labelText.Text = "tekst:";
            // 
            // labelEmail
            // 
            this.labelEmail.AutoSize = true;
            this.labelEmail.Location = new System.Drawing.Point(59, 183);
            this.labelEmail.Name = "labelEmail";
            this.labelEmail.Size = new System.Drawing.Size(37, 13);
            this.labelEmail.TabIndex = 6;
            this.labelEmail.Text = "e-mail:\r\n";
            // 
            // labelJttt
            // 
            this.labelJttt.AutoSize = true;
            this.labelJttt.Font = new System.Drawing.Font("Arial", 16F, System.Drawing.FontStyle.Bold);
            this.labelJttt.Location = new System.Drawing.Point(373, 18);
            this.labelJttt.Name = "labelJttt";
            this.labelJttt.Size = new System.Drawing.Size(69, 26);
            this.labelJttt.TabIndex = 7;
            this.labelJttt.Text = "JTTT";
            // 
            // listTasks
            // 
            this.listTasks.FormattingEnabled = true;
            this.listTasks.Location = new System.Drawing.Point(449, 77);
            this.listTasks.Name = "listTasks";
            this.listTasks.Size = new System.Drawing.Size(249, 147);
            this.listTasks.TabIndex = 8;
            // 
            // buttonStart
            // 
            this.buttonStart.Location = new System.Drawing.Point(449, 247);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(75, 49);
            this.buttonStart.TabIndex = 9;
            this.buttonStart.Text = "START";
            this.buttonStart.UseVisualStyleBackColor = true;
            // 
            // buttonClear
            // 
            this.buttonDeserialize.Location = new System.Drawing.Point(623, 273);
            this.buttonDeserialize.Name = "buttonClear";
            this.buttonDeserialize.Size = new System.Drawing.Size(75, 23);
            this.buttonDeserialize.TabIndex = 10;
            this.buttonDeserialize.Text = "Deserialize";
            this.buttonDeserialize.UseVisualStyleBackColor = true;
            // 
            // buttonSerialize
            // 
            this.buttonSerialize.Location = new System.Drawing.Point(623, 247);
            this.buttonSerialize.Name = "buttonSerialize";
            this.buttonSerialize.Size = new System.Drawing.Size(75, 23);
            this.buttonSerialize.TabIndex = 11;
            this.buttonSerialize.Text = "Serialize";
            this.buttonSerialize.UseVisualStyleBackColor = true;
            // 
            // buttonDeserialize
            // 
            this.buttonClear.Location = new System.Drawing.Point(534, 247);
            this.buttonClear.Name = "buttonDeserialize";
            this.buttonClear.Size = new System.Drawing.Size(75, 49);
            this.buttonClear.TabIndex = 12;
            this.buttonClear.Text = "Clear";
            this.buttonClear.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(726, 403);
            this.Controls.Add(this.buttonClear);
            this.Controls.Add(this.buttonSerialize);
            this.Controls.Add(this.buttonDeserialize);
            this.Controls.Add(this.buttonStart);
            this.Controls.Add(this.listTasks);
            this.Controls.Add(this.labelJttt);
            this.Controls.Add(this.labelEmail);
            this.Controls.Add(this.labelText);
            this.Controls.Add(this.labelUrl);
            this.Controls.Add(this.textBoxEmail);
            this.Controls.Add(this.textBoxText);
            this.Controls.Add(this.textBoxUrl);
            this.Controls.Add(this.buttonAdd);
            this.Name = "Form1";
            this.Text = "JTTT";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonAdd;
        private System.Windows.Forms.TextBox textBoxUrl;
        private System.Windows.Forms.TextBox textBoxText;
        private System.Windows.Forms.TextBox textBoxEmail;
        private System.Windows.Forms.Label labelUrl;
        private System.Windows.Forms.Label labelText;
        private System.Windows.Forms.Label labelEmail;
        private System.Windows.Forms.Label labelJttt;
        private System.Windows.Forms.ListBox listTasks;
        private System.Windows.Forms.Button buttonStart;
        private System.Windows.Forms.Button buttonDeserialize;
        private System.Windows.Forms.Button buttonSerialize;
        private System.Windows.Forms.Button buttonClear;
    }
}

