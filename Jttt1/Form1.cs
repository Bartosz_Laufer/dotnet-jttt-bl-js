﻿using System;
using System.Windows.Forms;
using System.Drawing.Imaging;

namespace Jttt1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void ButtonAdd_Click(object sender, EventArgs e)
        {
            TaskCondition jtttTaskCondition = new TaskCondition(textBoxUrl.Text, textBoxText.Text);
            TaskAction jtttTaskAction = new TaskAction(textBoxEmail.Text);

            try
            {
                if (String.IsNullOrEmpty(jtttTaskCondition.text))
                {
                    MessageBox.Show("Nie podano tekstu ktorego szukac na stronie");
                    LogFile.addToLogFile("Tried to search for image without specifying the text");
                }
                else
                {
                    jtttTaskCondition.FindImage();
                    if (String.IsNullOrEmpty(jtttTaskCondition.imageUrl))
                    {
                        MessageBox.Show("Nie znaleziono obrazka z podanym tekstem w tytule na tej stronie");
                        LogFile.addToLogFile("Image containing " + jtttTaskCondition.text + " has not been found on site " + jtttTaskCondition.siteUrl);
                    }
                    else
                    {
                        jtttTaskAction.imagePath = jtttTaskCondition.SaveImage(ImageFormat.Png);
                        jtttTaskAction.SendEmail();
                        MessageBox.Show("Obrazek zostal wyslany na podany adres e-mailowy");
                        LogFile.addToLogFile("Image containing " + jtttTaskCondition.text + " from site " + jtttTaskCondition.siteUrl + " has been sent to " + jtttTaskAction.email);
                    }
                }
            }

            catch (Exception exc)
            {
                if (exc is System.UriFormatException || exc is System.Net.WebException)
                {
                    MessageBox.Show("Podano niepoprawny lub nie podano adresu strony");
                    LogFile.addToLogFile("Incorrect site URL " + jtttTaskCondition.siteUrl);
                }
                else if (exc is System.ArgumentException || exc is System.FormatException)
                {
                    MessageBox.Show("Podano niepoprawny lub nie podanu adresu e-mailowego");
                    LogFile.addToLogFile("Incorrect e-mail address " + jtttTaskAction.email);
                }
                else
                {
                    MessageBox.Show("Wystapil blad: " + exc.Message);
                    LogFile.addToLogFile("Error: " + exc.Message);
                }
            }
        }
    }  


    /*
    public class TaskList : Task
    {
        BindingList<Task> tasks;
        public TaskList()
        {
            BindingList<Task> tasks = new BindingList<Task>();
            tasks.AllowNew = true;
            tasks.AllowRemove = false;
        }

        public Display()
        {
            
        }
    }
    */
    
}
