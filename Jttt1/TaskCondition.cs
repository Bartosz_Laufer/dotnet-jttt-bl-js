﻿using System;
using System.Drawing;
using System.Net;
using System.IO;
using System.Drawing.Imaging;
using HtmlAgilityPack;

namespace Jttt1
{
    public class TaskCondition
    {
        public string siteUrl;
        public string text;
        public string imageUrl;

        public TaskCondition()
        {
            siteUrl = "";
            text = "";
            imageUrl = "";
        }

        public TaskCondition(string url, string tekst)
        {
            siteUrl = url;
            text = tekst;
            imageUrl = "";
        }

        // funkcja zapisujaca obrazek ze strony na dysku
        public string SaveImage(ImageFormat format)
        {
            string imagePath = "image.jpeg";

            WebClient client = new WebClient();
            Stream stream = client.OpenRead(imageUrl);
            Bitmap bitmap;
            bitmap = new Bitmap(stream);

            if (bitmap != null)
                bitmap.Save(imagePath, format); // zapisanie obrazka w danym formacie

            stream.Flush();
            stream.Close();
            client.Dispose();

            return imagePath;
        }

        // funkcja znajdujaca obrazek na stronie
        public void FindImage()
        {
            HtmlAgilityPack.HtmlWeb web = new HtmlWeb();
            HtmlAgilityPack.HtmlDocument doc = web.Load(siteUrl);
            string imageUrl = ""; // zwracany link do znalezionego obrazka
            var nodes = doc.DocumentNode.Descendants("img"); // znalezienie wszystkich node'ow z nazwa img
            foreach (var node in nodes)
            {
                string title = node.GetAttributeValue("alt", ""); // tytul obrazka
                bool contains = title.IndexOf(text, StringComparison.OrdinalIgnoreCase) >= 0; // sprawdzenie czy tytul obrazka zawiera podany tekst
                if (contains == true)
                {
                    imageUrl = node.GetAttributeValue("src", ""); // pobranie linka do znalezionego obrazka
                    break; // zakonczenie poszukiwan
                }
            }
            this.imageUrl = imageUrl;
        }
    }
}
