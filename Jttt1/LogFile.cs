﻿using System;
using System.IO;

namespace Jttt1
{
    public class LogFile
    {
        // funkcja obslugujaca zapis do pliku log 
        public static void addToLogFile(string text)
        {
            string logFilePath = "../../jttt.log";
            DateTime now = DateTime.Now;

            if (!File.Exists(logFilePath))
            {
                string textOnCreation = now.ToString() + " Created log file" + Environment.NewLine;
                File.WriteAllText(logFilePath, textOnCreation); // utworzenie pliku i wpisanie pierwszej wiadomosci
            }

            string textToAdd = now.ToString() + " " + text + Environment.NewLine;
            File.AppendAllText(logFilePath, textToAdd); // wpisanie tekstu do pliku
        }
    }
}
