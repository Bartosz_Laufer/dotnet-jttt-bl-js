﻿using System;
using System.IO;
using System.Net.Mail;

namespace Jttt1
{
    public class TaskAction
    {
        public string email;
        public string imagePath;

        public TaskAction()
        {
            email = "";
            imagePath = "";
        }

        public TaskAction(string mail)
        {
            email = mail;
            imagePath = "";
        }

        // funkcja wysylajaca email
        public void SendEmail()
        {
            SmtpClient client = new SmtpClient();
            client.Port = 587;
            client.Host = "smtp.gmail.com";
            client.EnableSsl = true;
            client.Timeout = 10000;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Credentials = new System.Net.NetworkCredential("20jttt18@gmail.com", "dotnet123");

            MailMessage mail = new MailMessage();
            mail.To.Add(new MailAddress(email));
            mail.From = new MailAddress("20jttt18@gmail.com");
            mail.Subject = "Obrazek";
            mail.Body = "Dostarczono obrazek!";

            Attachment attachment = new Attachment(imagePath, System.Net.Mime.MediaTypeNames.Image.Jpeg); // dodanie zalcznika w postaci obrazka
            mail.Attachments.Add(attachment);

            client.Send(mail);

            client.Dispose();
            attachment.Dispose();
            File.Delete(imagePath);
        }
    }
}
